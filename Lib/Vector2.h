#pragma once
#include <compare>

class Vector2
{
public:
	Vector2() = default;
	Vector2(float x, float y) : m_x(x), m_y(y){}
	Vector2(const Vector2& v);
	Vector2& operator=(const Vector2& v);

	void x(const float& v)
	{
		m_x = v;
	}

	float x() const
	{
		return m_x;
	}

	void y(const float& v)
	{
		m_y = v;
	}

	float y() const
	{
		return m_y;
	}

	bool operator==(const Vector2& v) const;
	Vector2 operator+(const Vector2& v) const;

protected:
	float m_x = 0.0f;
	float m_y = 0.0f;
};

