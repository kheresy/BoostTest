#include "Vector2.h"

Vector2::Vector2(const Vector2& v)
{
	m_x = v.m_x;
	m_y = v.m_y;
}

Vector2& Vector2::operator=(const Vector2& v)
{
	m_x = v.m_x;
	m_y = v.m_y;
	return *this;
}

bool Vector2::operator==(const Vector2& v) const
{
	return (m_x ==v.m_x) && (m_y == v.m_y);
}

Vector2 Vector2::operator+(const Vector2& v) const
{
	return Vector2(m_x + v.m_x, m_y + v.m_y);
}
