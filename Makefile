all:
	cd Lib;		make;
	cd Test;	make;

clean:
	cd Lib;		make clean;
	cd Test;	make clean;
	rm -f *.a
