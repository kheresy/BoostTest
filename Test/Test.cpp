#define BOOST_TEST_MODULE Vec2Test
#include <boost/test/included/unit_test.hpp>

#include <iostream>

#include "Lib/Vector2.h"


std::ostream& operator<<(std::ostream& out, const Vector2& v)
{
	out << "(" << v.x() << "," << v.y() << ")";
	return out;
}

BOOST_AUTO_TEST_SUITE(Vec2_Test)

BOOST_AUTO_TEST_CASE(constructor)
{
	Vector2 a;
	BOOST_TEST_REQUIRE(a.x() == 0.0f);
	BOOST_TEST_REQUIRE(a.y() == 0.0f);

	Vector2 b(1.0f, 2.0f);
	BOOST_TEST_REQUIRE(b.x() == 1.0f);
	BOOST_TEST_REQUIRE(b.y() == 2.0f);

	Vector2 c(b);
	BOOST_TEST_REQUIRE(c == b);
}

BOOST_AUTO_TEST_CASE(compute)
{
	Vector2 a(1,2), b(2, 2), c(3,4);
	BOOST_TEST_REQUIRE(a + b == c);
}


BOOST_AUTO_TEST_SUITE_END()
